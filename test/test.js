var supertest = require("supertest");
var should = require("should");

describe("SAMPLE TEST esercizio_xpeppers" , function(){
	var server ; 

	beforeEach(function(){
		server = require("./../server");
	});

	afterEach(function (done) {
    	server.close();
    	done();
  	});

	it("GET / : Check index page" , function(done){
		supertest(server).get("/")
			.expect(200)
			.end(function(err , res){
				res.status.should.equal(200);
				done();
			});	
	});

	it("GET / : Check not found page" , function(done){
		supertest(server).get("/piposs")
			  .expect(404)
			  .expect("Accept" , "text/html")
			  .expect("Content-Type" , "html")
			  .end(function(err , res){
			  	res.status.should.equal(404);
			  	done();
			  });
	});

	it("POST /edit : Check update Error params" , function(done){
		supertest(server).post("/edit")
			  .expect(200)
			  .send({})
			  .expect("Content-type" , /json/)
			  .end(function(err , res){
			  	res.status.should.equal(200);
			  	res.body.error.should.equal("Invalid request");
			  	done();
			  });
	});

	it("GET /edit : Edit entris with not correctly id" , function(done){
		supertest(server).get('/edit/2034534')
			  .expect(404)
			  .expect("Content-Type" , "text/html")
			  .end(function(err , res){
			  		res.status.should.equal(404);
			  		done();
			  });
	});


	it("GET /add : Return to add page " , function(done){
		supertest(server).get("/add")
						 .expect(200)
			  			 .expect("Content-Type" , "text/html")
			             .end(function(err , res){
			  		         res.status.should.equal(200);
			  				 done();
			  			});
	});

	it("POST /add : Request to add new entries with not correctly params" , function(done){
			supertest(server).post("/add")
							.expect(404)
							.expect("Content-Type" , "text/html")
							.send({frist_name:"Mammals"})
							.end(function(err , res){
								res.status.should.equal(404);
								done();				
							});	
	});

	it("POST /add : Request to add new entries with  correctly params" , function(done){
			var entries = [
				{
					frist_name : "Marco" , 
					last_name : "Carta",
					telephone_number : "+39 29 38238232"
				},
				{
					frist_name : "Giovanni" , 
					last_name : "Lamorra",
					telephone_number : "+39 26 2372362362"
				},
				{
					frist_name : "Eleonora" , 
					last_name : "Oriolos",
					telephone_number : "+98 38 4374723734"
				},
				{
					frist_name : "Andrea Morcass" , 
					last_name : "Monaco",
					telephone_number : "+02 39 38438248"
				},
				{
					frist_name : "Pollo" , 
					last_name : "Cotto",
					telephone_number : "+09 38 23883223"
				},
				{
					frist_name : "skober" , 
					last_name : "moker",
					telephone_number : "+29 83 432842334"
				}
			];
			supertest(server).post("/add")
							.send(entries[Math.floor(Math.random() * entries.length)])
							.end(function(err , res){
								if (err) done(err);
								else done();
												
							});	
	});


});