/*
	Define module entrie on mongoose 
*/
var mongoose = require("mongoose");

var EntrieSchema = new mongoose.Schema({
	frist_name : { type : String , required : "frist name is required" , validate : /^[a-z\s]+$/i } , 
	last_name : { type : String , required : "last name is required" , validate : /^[a-z\s]+$/i } , 
	telephone_number : { type : String , required : "telephone number is required" , validate : /\+[0-9]+( )[0-9]+( )[0-9]{6,}$/}
});

module.exports = mongoose.model("Entrie" , EntrieSchema);