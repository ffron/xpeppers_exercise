var express = require("express");
var mongoose = require("mongoose");
var bodyparser = require("body-parser");
var Entries = require("./models/entrie");
var morgan = require("morgan");
var EntriesRoutes = require("./routes/routes_entries.js");
var exphbs  = require('express-handlebars');

var app = express();

//MIDW
app.use('/public',express.static(__dirname + '/public'));
app.engine('handlebars', exphbs({defaultLayout: 'base'}));
app.set('view engine', 'handlebars');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(morgan('combined'));

//CONNECT TO DATABSE
var DATABASE_URI = 'mongodb://localhost:27017/xpappers' ; 
mongoose.connect(DATABASE_URI);

mongoose.connection.on('connected' , function(){
	console.log("Mongoose connect at url : " + DATABASE_URI);
});
mongoose.connection.on('error' , function(err){
	console.log("Mongoose connect error : " + err);
});



app.use("/" , EntriesRoutes);
//Error page not found 
app.use(function(req ,res){
	res.status(404).render("error" , { title : "Page not found 404" });
});


port = Number(process.env.PORT || 8000); 
var server = app.listen(port);

module.exports = server ; 