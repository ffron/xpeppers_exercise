# README #

# Esercizio xpeppers #

* E' stato realizzato utilizzando :  mongoose , express , nodejs , bootstrap , Mocha , HandleBars . 
* Link a heroku : https://xpeppersex.herokuapp.com/

## Istallazione ##

E' necessario aver installato npm,nodejs,mongodb alle versioni più recenti. Eseguire un istanza di mongodb. 
Per eseguire i test è necessario avere installato in modo globale Mocha:
```
#!shell

sudo npm install mocha -g
```
 Dopo aver eseguito i passi precedenti basta clonare il repository 
```
#!shell

git clone https://bitbucket.org/ffron/xpeppers_esercizio.git
```
 eseguire il seguente comando  per installare le dipendenze necessarie
```
#!shell

npm install 
```

per eseguire l'app basta digitare 
```
#!shell

npm start
```
in seguito si accede in locale :  http://localhost:8000 . per eseguire i pochi test presenti 
```
#!shell

npm test
```
.

## Info ##
Creato da Francesco Frontera , 2016.