$(document).ready(function() {
    $('#regexpForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	frist_name : {
        		validators : {
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The frist name can consist of alphabetical characters and spaces only'
                    } , 
        			notEmpty : {
        				message : "The frist name is required!"
        			}
        		}
        	},
        	last_name : {
        		validators : {
                     regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The last name can consist of alphabetical characters and spaces only'
                    } ,
        			notEmpty : {
        				message : "The last name is required!"
        			}
        		}
        	},
            telephone_number :  {
                validators: {
                	notEmpty : {
                		message : "The number is required!"
                	},
                    regexp: {
                        regexp: /\+[0-9]+( )[0-9]+( )[0-9]{6,}$/,
                        message: 'The number format is +39 02 0294654 ,the last group must contain a minimum of 6 numbers ',
                    }
                }
            }
        }
    });
});