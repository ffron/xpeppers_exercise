var express = require('express');  
var router = express.Router();
var Entries = require("./../models/entrie");



router.get('/' , function(req , res){
	Entries.find({} , function( err , docs ){
		//console.log(docs);
		res.render("index", {entries : docs , title : "xpeppers home" , details : "list entries"});
	});

});


router.get('/add' , function(req , res){
	res.render("add" , {title : "xpeppers add entries" , details : "From to add new entrie"});
});
 
router.post("/add" , function( req , res ){
		var new_obj = new Entries(req.body);
		new_obj.save(function(err , data){
			if(err){
				//console.log(err);
			 	res.status(404).render("error" , { error : err });	
			}else{
				res.redirect("/");
			}	
		});	
});


router.get("/edit/:id" , function(req , res ){
	console.log("router /edit/:id");
	Entries.findOne( {'_id' : req.params.id } , function(err , data ){
		if(err){
			res.status(404).render("error" ,{ title : "Page not found 404"  });
		}else{
			res.render("edit" , 
				{
					title : "Xpeppers Edit" , 
					details  : "Edit " + data.frist_name + " " + data.last_name , 
					frist_name : data.frist_name , 
					last_name : data.last_name , 
					telephone_number : data.telephone_number, 
					id : data._id
				});
		}	
	});
});

router.post("/edit" , function(req , res){
	if(req.body.id && req.body.frist_name && req.body.last_name && req.body.telephone_number){
		var query = { '_id' : req.body.id };
		var updater = {frist_name : req.body.frist_name , last_name :req.body.last_name , telephone_number : req.body.telephone_number  };
		Entries.update(query , updater , { multi : false }, function(err , data){
			if( err ) res.status(400).render("error" , { title : "Page not found 404" , details : err });
			else{
				console.log(data);
				res.redirect("/");
			}
		});	
	}else{
		res.json({error : "Invalid request"});
	}	
});


module.exports = router;